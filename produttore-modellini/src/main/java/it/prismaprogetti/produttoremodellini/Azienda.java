package it.prismaprogetti.produttoremodellini;

import it.prismaprogetti.produttoremodellini.modelli.Colore;
import it.prismaprogetti.produttoremodellini.modelli.Modello;

public class Azienda {

	private Modellino[] listaModellini;
	
	/**
	 * Costruttore di default
	 */
	
	/**
	 * Metodo che traduce i numeri del traccaito tipologia in Modellini
	 * @param catalogo
	 * @return
	 */
	public Modellino[] elaboraModellini(Catalogo catalogo) {
		
		int lunghezzaListaModelliniDaprodurre = catalogo.getTracciatoTipologia().getArrayTracciatoTipologia().length;
		int contatore = 0;
		
		this.listaModellini = new Modellino[lunghezzaListaModelliniDaprodurre];
		
		while(contatore <= lunghezzaListaModelliniDaprodurre-1) {
			
			/*
			 * Attraverso il catalogo mi prendo l'array contenente, in quella data posizione, il numero modello
			 * che sarà l'indice dell'array che continene i tipi di modello quindi posso dare al modellinno il modello relatizo a quella posizione
			 * data da quell'indice
			 */
			int indiceModello = catalogo.getTracciatoTipologia().getArrayTracciatoTipologia()[contatore].getNumeroModello();
			Modello modelloTradotto = catalogo.getTracciatoModelli().getModelli()[indiceModello];
			/*
			 * Stessa cosa del Modello per il colore
			 */
			int indiceColore = catalogo.getTracciatoTipologia().getArrayTracciatoTipologia()[contatore].getNumeroColore();
			Colore coloreTradotto = catalogo.getTracciatoColori().getColori()[indiceColore];
			/*
			 *Per i numeri pezzi mi prendo direttamente i numeri dei pezzi
			 */
			int numeroPezzi = catalogo.getTracciatoTipologia().getArrayTracciatoTipologia()[contatore].getNumeroPezzi();
			this.listaModellini[contatore] = new Modellino(modelloTradotto, coloreTradotto, numeroPezzi);
			
			contatore++;
		}
		
		return this.listaModellini;
		
	}
	
	public Modellino[] listaModelliniPerModello(Modello modello) {
		
		int contatore = 0;
		int quantitaModelliniTrovatiFerrari = 0;
		int quantitaModelliniTrovatiMaserati = 0;
		int quantitaModelliniTrovatiPorche = 0;
		
		/*
		 * Devo fare prima questo passaggio per avere il numero di modelli uguali a quello che mi è stato passato come parametro
		 * per sapere effettivamente quanti di quel modello ce ne sono dentro la lista di modellini
		 * così poi da potre creare un array di quella lunghezza dove andare a mettere all'iterno solo i modellini di quel tipo di modello
		 */
		while(contatore <= (this.listaModellini.length-1)) {
			if(this.listaModellini[contatore].getModello().getModelloFerrari()!=null 
				&&this.listaModellini[contatore].getModello().getModelloFerrari().equals(modello.getModelloFerrari())
				&&this.listaModellini[contatore].getModello().getMarca().equals(modello.getMarca())) {
				quantitaModelliniTrovatiFerrari++;
				contatore++;
			}else if(this.listaModellini[contatore].getModello().getModelloMaserati()!=null 
					&&this.listaModellini[contatore].getModello().getModelloMaserati().equals(modello.getModelloMaserati())
					&&this.listaModellini[contatore].getModello().getMarca().equals(modello.getMarca())) {
				quantitaModelliniTrovatiMaserati++;
				contatore++;
			}else if(this.listaModellini[contatore].getModello().getModelloPorche()!=null 
					&&this.listaModellini[contatore].getModello().getModelloPorche().equals(modello.getModelloPorche())
					&&this.listaModellini[contatore].getModello().getMarca().equals(modello.getMarca())) {
				quantitaModelliniTrovatiPorche++;
				contatore++;
			}else {
				contatore++;
			}
		}
		
		Modellino[] modelliniPerTipoModello = null;
		
		if(quantitaModelliniTrovatiFerrari!=0) {
			modelliniPerTipoModello = new Modellino[quantitaModelliniTrovatiFerrari];
			contatore=0;
			int contatoreIndiceModelliniFerrari = 0;
			
			while(contatore <= (this.listaModellini.length-1)) {
				
				if(this.listaModellini[contatore].getModello().getModelloFerrari()!=null 
					&&this.listaModellini[contatore].getModello().getModelloFerrari().equals(modello.getModelloFerrari())
					&&this.listaModellini[contatore].getModello().getMarca().equals(modello.getMarca())) {
					
					modelliniPerTipoModello[contatoreIndiceModelliniFerrari] = this.listaModellini[contatore];
					contatoreIndiceModelliniFerrari++;
					contatore++;
				}else {
					contatore++;
				}
			}
			
		}else if(quantitaModelliniTrovatiMaserati!=0) {
			
			modelliniPerTipoModello = new Modellino[quantitaModelliniTrovatiMaserati];
			contatore=0;
			int contatoreIndiceModelliniMaserati = 0;
			
			while(contatore <= (this.listaModellini.length-1)) {
				
				if(this.listaModellini[contatore].getModello().getModelloMaserati()!=null 
					&& this.listaModellini[contatore].getModello().getModelloMaserati().equals(modello.getModelloMaserati())
					&&this.listaModellini[contatore].getModello().getMarca().equals(modello.getMarca())) {
					
					modelliniPerTipoModello[contatoreIndiceModelliniMaserati] = this.listaModellini[contatore];
					contatoreIndiceModelliniMaserati++;
					contatore++;
				}else {
					contatore++;
				}
			}
			
		}else if(quantitaModelliniTrovatiPorche!=0) {
			modelliniPerTipoModello = new Modellino[quantitaModelliniTrovatiMaserati];
			contatore=0;
			int contatoreIndiceModelliniPorche = 0;
			
			while(contatore <= (this.listaModellini.length-1)) {
				
				if(this.listaModellini[contatore].getModello().getModelloPorche()!=null 
					&&this.listaModellini[contatore].getModello().getModelloPorche().equals(modello.getModelloPorche())
					&&this.listaModellini[contatore].getModello().getMarca().equals(modello.getMarca())) {
					
					modelliniPerTipoModello[contatoreIndiceModelliniPorche] = this.listaModellini[contatore];
					contatoreIndiceModelliniPorche++;
					contatore++;
				}else {
					contatore++;
				}
			}
		}
		
		
		return modelliniPerTipoModello;
	}
	
	public Modellino[] listaModelliniPerColore(Colore colore) {
		
		/*
		 * TODO
		 */
		
		int contatore = 0;
		int contatoreQuantitaColoriUguali = 0;
		
		while(contatore <= (this.listaModellini.length-1)) {
			
			if(this.listaModellini[contatore].getColore().equals(colore)) {
				contatoreQuantitaColoriUguali++;
				contatore++;
			}else {
				contatore++;
			}
		}
		contatore = 0;
		Modellino[] modelliniPerColore = new Modellino[contatoreQuantitaColoriUguali];
		int contatoreIndiceModelliniPerColore = 0;
		
		while(contatore <= (this.listaModellini.length-1)){			
			if(this.listaModellini[contatore].getColore().equals(colore)) {
			
				modelliniPerColore[contatoreIndiceModelliniPerColore] = this.listaModellini[contatore];
				contatoreIndiceModelliniPerColore++;
				contatore++;
			}else {
				contatore++;
			}
			
		}
		
		return modelliniPerColore;
	}
	
	public Modellino[] listaModelliniPerNumeroMinPezzi(int numeroMinPezzi) {
		
		int contatore = 0;
		int contatoreQuantitaPezziMinimi = 0;
		
		while(contatore <= (this.listaModellini.length-1)) {
			
			if(this.listaModellini[contatore].getNumeroPezzi() >= numeroMinPezzi ) {
				contatoreQuantitaPezziMinimi++;
				contatore++;
			}else {
				contatore++;
			}
		}
		contatore = 0;
		Modellino[] modelliniPerNumeroPezziMinimi = new Modellino[contatoreQuantitaPezziMinimi];
		int contatoreIndiceModelliniPerPezziMinimi = 0;
		
		while(contatore <= (this.listaModellini.length-1)){			
			if(this.listaModellini[contatore].getNumeroPezzi() >= numeroMinPezzi) {
			
				modelliniPerNumeroPezziMinimi[contatoreIndiceModelliniPerPezziMinimi] = this.listaModellini[contatore];
				contatoreIndiceModelliniPerPezziMinimi++;
				contatore++;
			}else {
				contatore++;
			}
		}
		
		return modelliniPerNumeroPezziMinimi;
	}
	
}
