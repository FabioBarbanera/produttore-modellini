package it.prismaprogetti.produttoremodellini;

import it.prismaprogetti.produttoremodellini.tracciati.TracciatoColori;
import it.prismaprogetti.produttoremodellini.tracciati.TracciatoModelli;
import it.prismaprogetti.produttoremodellini.tracciati.TracciatoTipologia;

public class Catalogo {

	private TracciatoTipologia tracciatoTipologia;
	private TracciatoModelli tracciatoModelli;
	private TracciatoColori tracciatoColori;
	
	private Catalogo(TracciatoTipologia tracciatoTipologia, TracciatoModelli tracciatoModelli, TracciatoColori tracciatoColori) {
		this.tracciatoTipologia = tracciatoTipologia;
		this.tracciatoModelli = tracciatoModelli;
		this.tracciatoColori = tracciatoColori;
	}
	
	public static Catalogo crea(TracciatoModelli tracciatoModelli, TracciatoColori tracciatoColori, int numeroMinPezzi, int numeroMaxPezzi, int numeroQuantitaModelliniDaCreare) {

		TracciatoTipologia tracciatotipologia = null;

		/*
		 * Con le lunghezze degli altri due tracciati posso creare il tracciato tipologia
		 */
		tracciatotipologia = TracciatoTipologia.crea((tracciatoModelli.getModelli().length-1), (tracciatoColori.getColori().length-1), numeroMinPezzi, numeroMaxPezzi, numeroQuantitaModelliniDaCreare);
		
		return new Catalogo(tracciatotipologia, tracciatoModelli, tracciatoColori);
	}

	public TracciatoTipologia getTracciatoTipologia() {
		return tracciatoTipologia;
	}

	public TracciatoModelli getTracciatoModelli() {
		return tracciatoModelli;
	}

	public TracciatoColori getTracciatoColori() {
		return tracciatoColori;
	}
	
	
	
}
