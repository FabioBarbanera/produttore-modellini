package it.prismaprogetti.produttoremodellini.modelli;

public class Modello {

	private Marca marca;
	private ModelloFerrari modelloFerrari;
	private ModelloMaserati modelloMaserati;
	private ModelloPorche modelloPorche;
	
	public Modello(Marca marca, ModelloFerrari modelloFerrai) {
		this.marca = marca;
		this.modelloFerrari = modelloFerrai;
	}
	
	public Modello(Marca marca, ModelloMaserati modelloMaserati) {
		this.marca = marca;
		this.modelloMaserati = modelloMaserati;
	}
	
	public Modello(Marca marca, ModelloPorche modelloPorche) {
		this.marca = marca;
		this.modelloPorche = modelloPorche;
	}

	public Marca getMarca() {
		return marca;
	}

	public ModelloFerrari getModelloFerrari() {
		return modelloFerrari;
	}

	public ModelloMaserati getModelloMaserati() {
		return modelloMaserati;
	}

	public ModelloPorche getModelloPorche() {
		return modelloPorche;
	}

	@Override
	public String toString() {
		if(marca.equals(Marca.FERRARI)) {
			return marca + " " + modelloFerrari;
		}else if(marca.equals(Marca.MASERATI)) {
			return marca + " " + modelloMaserati;
		}
			return marca + " " + modelloPorche;
	}

	
	
}
