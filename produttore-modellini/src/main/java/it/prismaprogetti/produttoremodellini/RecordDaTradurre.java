package it.prismaprogetti.produttoremodellini;

public class RecordDaTradurre {

	private int numeroModello;
	private int numeroColore;
	private int numeroPezzi;
	
	public RecordDaTradurre(int numeroModello, int numeroColore, int numeroPezzi) {
		this.numeroModello = numeroModello;
		this.numeroColore = numeroColore;
		this.numeroPezzi = numeroPezzi;
	}

	public int getNumeroModello() {
		return numeroModello;
	}

	public int getNumeroColore() {
		return numeroColore;
	}

	public int getNumeroPezzi() {
		return numeroPezzi;
	}
	
}
