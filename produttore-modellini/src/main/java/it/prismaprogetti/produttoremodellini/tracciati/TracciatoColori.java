package it.prismaprogetti.produttoremodellini.tracciati;

import it.prismaprogetti.produttoremodellini.modelli.Colore;

public class TracciatoColori {

	private Colore[] colori;

	private TracciatoColori(Colore[] colori) {
		this.colori = colori;
	}

	public Colore[] getColori() {
		return colori;
	}
	
	public static TracciatoColori crea(Colore[]colori) {
		
		return new TracciatoColori(colori);
	}
	
}
