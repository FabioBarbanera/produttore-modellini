package it.prismaprogetti.produttoremodellini.tracciati;

import it.prismaprogetti.produttoremodellini.RecordDaTradurre;
import it.prismaprogetti.produttoremodellini.utility.NumeroRandom;

public class TracciatoTipologia {

	private RecordDaTradurre[] arrayTracciatoTipologia;

	private TracciatoTipologia(RecordDaTradurre[] arrayTracciatoTipologia) {
		this.arrayTracciatoTipologia = arrayTracciatoTipologia;
	}

	public RecordDaTradurre[] getArrayTracciatoTipologia() {
		return arrayTracciatoTipologia;
	}
	
	public static TracciatoTipologia crea(int numeroMaxModelli, int numeroMaxColori, int numeroMinPezzi, int numeroMaxPezzi, int numeroQuantitaModelliniDaCreare) {
		
		
		int numeroLunghezzaTracciato = numeroQuantitaModelliniDaCreare;
		int contatore = 0;
		RecordDaTradurre[] arrayTracciatoTipologia = null;
		
		arrayTracciatoTipologia = new RecordDaTradurre[numeroLunghezzaTracciato];
		
		while(contatore<=numeroLunghezzaTracciato-1) {
	
			int numeroRandomModello = NumeroRandom.crea(0, numeroMaxModelli);
			int numeroRandomColore = NumeroRandom.crea(0, numeroMaxColori);
			int numeroPezzi = NumeroRandom.crea(numeroMinPezzi, numeroMaxPezzi);
		
			RecordDaTradurre record = new RecordDaTradurre(numeroRandomModello, numeroRandomColore, numeroPezzi);
			
			arrayTracciatoTipologia[contatore] = record;
			
			contatore++;
		}
		
		return new TracciatoTipologia(arrayTracciatoTipologia);
		
	}
	
}
