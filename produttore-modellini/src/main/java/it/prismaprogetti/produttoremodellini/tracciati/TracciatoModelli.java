package it.prismaprogetti.produttoremodellini.tracciati;

import it.prismaprogetti.produttoremodellini.modelli.Marca;
import it.prismaprogetti.produttoremodellini.modelli.Modello;
import it.prismaprogetti.produttoremodellini.modelli.ModelloFerrari;
import it.prismaprogetti.produttoremodellini.modelli.ModelloMaserati;
import it.prismaprogetti.produttoremodellini.modelli.ModelloPorche;

public class TracciatoModelli {

	private Modello[] modelli;

	private TracciatoModelli(Modello[] modelli) {
		this.modelli = modelli;
	}

	public Modello[] getModelli() {
		return modelli;
	}
	
	public static TracciatoModelli crea(ModelloFerrari[]modelliFerrari, ModelloMaserati[] modelliMaserati, ModelloPorche[] modelliPorche, Marca[] marca) {
		
		
		int contatore = 0;
		int contatoreModelliFerrari = 0;
		int contatoreModelliMaserati = 0;
		int contatoreModelliPorche = 0;
		int contatoreMarca = 0;
		int lunghezzaTracciatoModelli = modelliFerrari.length + modelliMaserati.length + modelliPorche.length;
		Modello[] tracciatoModelli = new Modello[lunghezzaTracciatoModelli];
		
		while(contatore <= (lunghezzaTracciatoModelli-1)) {
			
			
			/*	
			 * Per creare il tracciatro dei modelli devo prima crearmi i modelli, ogni singolo modello deve essere comprendente
			 * di marca (es.Ferrari) e modello(e. portofino), queste due creano un modello
			 */
			
			/*
			 * Quindi devo farlo per tutti e tre le marche, 
			 * devo crearmi un modello per ogni tipo relativo a quella marca
			 */
				while(contatoreModelliFerrari <= (modelliFerrari.length-1)) {
					
					Modello modelloFerrari = new Modello(marca[contatoreMarca], modelliFerrari[contatoreModelliFerrari]);
					
					tracciatoModelli[contatore] =  modelloFerrari;
					
					contatoreModelliFerrari++;
					contatore++;
				}
				contatoreMarca++;
				
				while(contatoreModelliMaserati <= (modelliMaserati.length-1)) {
					
					Modello modelloMaserati = new Modello(marca[contatoreMarca], modelliMaserati[contatoreModelliMaserati]);
					
					tracciatoModelli[contatore] =  modelloMaserati;
					
					contatoreModelliMaserati++;
					contatore++;
				}
				contatoreMarca++;
				
				while(contatoreModelliPorche <= (modelliPorche.length-1)) {
					
					Modello modelloPorche = new Modello(marca[contatoreMarca], modelliPorche[contatoreModelliPorche]);
					
					tracciatoModelli[contatore] =  modelloPorche;
					
					contatoreModelliPorche++;
					contatore++;
				}
		}
		
		return new TracciatoModelli(tracciatoModelli);
	}
	
}
