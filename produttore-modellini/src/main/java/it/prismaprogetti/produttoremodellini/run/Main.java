package it.prismaprogetti.produttoremodellini.run;

import it.prismaprogetti.produttoremodellini.Azienda;
import it.prismaprogetti.produttoremodellini.Catalogo;
import it.prismaprogetti.produttoremodellini.Modellino;
import it.prismaprogetti.produttoremodellini.modelli.Colore;
import it.prismaprogetti.produttoremodellini.modelli.Marca;
import it.prismaprogetti.produttoremodellini.modelli.ModelloFerrari;
import it.prismaprogetti.produttoremodellini.modelli.ModelloMaserati;
import it.prismaprogetti.produttoremodellini.modelli.ModelloPorche;
import it.prismaprogetti.produttoremodellini.tracciati.TracciatoColori;
import it.prismaprogetti.produttoremodellini.tracciati.TracciatoModelli;
import it.prismaprogetti.produttoremodellini.utility.NumeroRandom;

public class Main {

	public static void main(String[] args) {

		/*
		 * Prima mi creo i due tracciati modelli e colori poi sulla base di quelli il catalogo, il quale all'interno, sulla base dei due tracciati passati,
		 * mi crea il catalogo sulla base del tracciato tipologia, quindi il catalogo potrà essere creato in manierà corrente sulla base del tracciato tipologia
		 */
		
		TracciatoModelli tarciattoModelli = TracciatoModelli.crea(ModelloFerrari.values(), ModelloMaserati.values(), ModelloPorche.values(), Marca.values());
		TracciatoColori tracciatoColori = TracciatoColori.crea(Colore.values());
		
		int numeroModelliniDaCreare = NumeroRandom.crea(3, 90);
		
		/*
		 * Questi potrebbero essere calcolati randomicamente quando creo il catalogo, 
		 * perché nel catalogo potrebbero essere definiti il numero di pezzi massimi e minimi
		 */
		int numeroMinPezzi = 3;
		int numeroMaxPezzi = 10;
		/*
		 * Il record essendo composto da 3 fields, la lunghezza dell'array contenente i records deve per forza essere un numero
		 * che se diviso per 3 da resto 0
		 */
		/*
		 * Questo controllo del diviso 3 con resto zero andava fatto nel caso non avessi avuto un oggetto recrod
		 * Ma nel tracciato tipologia avessi avuto in ordine il modello, il colore ed il numero dei pezzi,
		 * se fosse uscitro un numero che diviso per tre non da resto 0 sarebbero mancati dei pezzi 
		 */
//		while(numeroModelliniDaCreare%3!=0){
//			numeroModelliniDaCreare = NumeroRandom.crea(3, 90);
//		}
		numeroModelliniDaCreare = NumeroRandom.crea(3, 90);
		Catalogo catalogo = Catalogo.crea(tarciattoModelli, tracciatoColori, numeroMinPezzi, numeroMaxPezzi, numeroModelliniDaCreare);
		
		/*
		 * Il catalogo potrebbe essere passato al momenti della creazione dell'azienda
		 */
		Azienda azienda = new Azienda();
		
		Modellino[] listaModellini = azienda.elaboraModellini(catalogo);
		
		for (Modellino modellino : listaModellini) {
			System.out.println(modellino);
		}
		
		/*
		 * Per restituire i modellini con certe caratteristiche, basta fare dei metodi che prendendo in input quei dati
		 * poi scorrendo la lista con delle condizioni dedicate mi restituiscono i modellini relativi ai dati immessi
		 * Questo lo può fare l'azienda
		 */
//		azienda.elaboraModellini(catalogo);
		
		/*
		 * Ricerca per modello
		 */
//		Modello modello = new Modello(Marca.FERRARI, ModelloFerrari.PORTOFINO);
//		
//		Modellino[] modelliniTrovatiPerModello = azienda.listaModelliniPerModello(modello);
//		
//		for (Modellino modellino : modelliniTrovatiPerModello) {
//			System.out.println(modellino);
//		}
		
		/*
		 * Ricerca per colore
		 */
//		Colore colrePerRicerca = Colore.BIANCO;
//		
//		Modellino[] modelliniTrovatiPerColore = azienda.listaModelliniPerColore(colrePerRicerca);
//		
//		
//		for (Modellino modellino : modelliniTrovatiPerColore) {
//			System.out.println(modellino);
//		}
		
		/*
		 * Ricerca per numero minimo di pezzi
		 */
//		int numeroPezziMinimiPerRicerca = 4;
//		
//		Modellino[] modelliniTrovatiPerNumeroMinimoPezzi = azienda.listaModelliniPerNumeroMinPezzi(numeroPezziMinimiPerRicerca);
//		
//		for (Modellino modellino : modelliniTrovatiPerNumeroMinimoPezzi) {
//			System.out.println(modellino);
//		}
		
	}
}
