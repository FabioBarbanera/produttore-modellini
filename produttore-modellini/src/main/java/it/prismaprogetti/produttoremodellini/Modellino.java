package it.prismaprogetti.produttoremodellini;

import it.prismaprogetti.produttoremodellini.modelli.Colore;
import it.prismaprogetti.produttoremodellini.modelli.Modello;

public class Modellino {

	private Modello modello;
	private Colore colore;
	private int numeroPezzi;
	
	public Modellino(Modello modello, Colore colore, int numeroPezzi) {
		this.modello = modello;
		this.colore = colore;
		this.numeroPezzi = numeroPezzi;
	}

	public Modello getModello() {
		return modello;
	}

	public Colore getColore() {
		return colore;
	}

	public int getNumeroPezzi() {
		return numeroPezzi;
	}

	@Override
	public String toString() {
		return "Modellino [modello=" + modello + ", colore=" + colore + ", numeroPezzi=" + numeroPezzi + "]\n";
	}

	
	
}
