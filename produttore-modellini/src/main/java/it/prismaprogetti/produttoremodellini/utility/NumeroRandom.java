package it.prismaprogetti.produttoremodellini.utility;

import java.util.Random;

public class NumeroRandom {

	public static int crea(int min, int max) {
		
		Random random = new Random();
		int numeroRandom = random.nextInt(max - min) + min;
		
		return numeroRandom;
	}
}
